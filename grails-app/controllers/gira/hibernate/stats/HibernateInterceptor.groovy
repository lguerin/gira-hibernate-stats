package gira.hibernate.stats

import org.hibernate.SessionFactory
import org.hibernate.stat.Statistics
import org.springframework.beans.factory.annotation.Value


class HibernateInterceptor {

    @Value('${gira.hibernate.stats.enabled:false}')
    private Boolean enabled

    SessionFactory sessionFactory

    private static final String START_TIME = 'ACTION_START_TIME'

    public HibernateInterceptor() {
        match(uri: '/api/**')
    }

    boolean before() {
        if (!enabled) {
            return true
        }

        request[START_TIME] = System.currentTimeMillis()

        // Force to enable Hibernate stats
        Statistics stats = sessionFactory.getStatistics()
        if (!stats.statisticsEnabled) {
            stats.statisticsEnabled = true
        }

        return true
    }

    boolean after() { true }

    void afterView() {
        if (!enabled) {
            return
        }

        Long start = request[START_TIME] as Long
        Long end = System.currentTimeMillis()

        Statistics stats = sessionFactory.getStatistics()
        log.info """
  ================== Gira stats ==================
  Controller: [$controllerName / $actionName]
  Queries count: $stats.queryExecutionCount
  Transaction count: $stats.transactionCount
  Flush count: $stats.flushCount
  Entity load count: $stats.entityLoadCount
  Collection load count: $stats.collectionLoadCount
  Query execution max time: $stats.queryExecutionMaxTime ms
  Entity cache hit count: $stats.naturalIdCacheHitCount
  Query cache hit count: $stats.queryCacheHitCount
  L2 cache hit count: $stats.secondLevelCacheHitCount
  Update timestamp cache hit count: $stats.updateTimestampsCacheHitCount
  Elapsed time: ${end - start} ms
  =================================================
    
"""

        // Reset stats
        stats.clear()
    }
}
